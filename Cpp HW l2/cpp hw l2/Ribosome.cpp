#include "Ribosome.h"

using namespace std;

Protein* Ribosome::create_protein(std::string& RNA_transcript) const
{
	string sub_rna = "";
	Protein* protein = new Protein;

	protein->init();

	for (int i = 0; i < RNA_transcript.size(); i+=3)
	{
		sub_rna = RNA_transcript.substr(i, 3);

		if (get_amino_acid(sub_rna) == UNKNOWN)
		{
			protein->clear();
			return nullptr;
		}
		else
		{
			protein->add(get_amino_acid(sub_rna));
		}
	}

	return protein;
}