#include "Cell.h"

void Cell::init(const std::string dna_sequence, const Gene glucose_receptor_gene)
{
	_glucose_receptor_gene.init(glucose_receptor_gene.get_start(), glucose_receptor_gene.get_end(), glucose_receptor_gene.is_on_complementary_dna_strand());
	_nucleus.init(dna_sequence);
	_mitochondrion.init();
}

bool Cell::get_ATP()
{
	const Gene &gene = _glucose_receptor_gene;
	string rna = _nucleus.get_RNA_transcript(gene);
	Protein* protein;
	bool atp = false;


	protein = _ribosome.create_protein(rna);

	if (protein == nullptr)
	{
		cerr << "Didnt manage to create protein" << endl;
		_exit(1);
	}

	const Protein& copy_protein = *protein;
	_mitochondrion.insert_glucose_receptor(copy_protein);
	_mitochondrion.set_glucose(50);
	atp = _mitochondrion.produceATP();


	if (atp == false)
	{
		return false;
	}
	else
	{
		_atp_units = 100;
		return true;
	}
}