#include <iostream>
#include <string>
#include "Nucleus.h"

using namespace std;

void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
{
	this->_start = start;
	this->_end = end;
	this->_on_complementary_dna_strand = _on_complementary_dna_strand;
}

unsigned int Gene::get_start() const
{
	return _start;
}

unsigned int Gene::get_end() const
{
	return _end;
}

bool Gene::is_on_complementary_dna_strand() const
{
	return _on_complementary_dna_strand;
}

void Gene::set_start(unsigned int start)
{
	_start = start;
}

void Gene::set_end(unsigned int end)
{
	_end = end;
}

void Gene::set_on_complementary_dna_strand(bool _on_complementary_dna_strand)
{
	this->_on_complementary_dna_strand = _on_complementary_dna_strand;
}

void Nucleus::init(string dna_sequence)
{
	string _copy_dna = "";


	_DNA_strand = dna_sequence;

	for (int i = 0; i < dna_sequence.size(); i++)
	{
		switch (dna_sequence[i])
		{
		case 'G':
			_copy_dna += 'C';
		break;
		case 'C':
			_copy_dna += 'G';
		break;
		case 'A':
			_copy_dna += 'T';
		break;
		case 'T':
			_copy_dna += 'A';
		break;
		default:
			cerr << "The char must be C or G or T or A" << endl;
			_exit(1);
		break;
		}
	}

	_complementary_DNA_strand = _copy_dna;
}

string Nucleus::get_RNA_transcript(const Gene& gene) const
{
	string _rna = "";


	if (gene.is_on_complementary_dna_strand() == true)
	{
		for (int i = gene.get_start(); i <= gene.get_end(); i++)
		{
			
			_rna += _complementary_DNA_strand[i];
			
		}
	}
	else
	{
		for (int i = gene.get_start(); i <= gene.get_end(); i++)
		{
			_rna += _DNA_strand[i];

		}
	}


	for (int i = 0; i < _rna.size(); i++)
	{
		if (_rna[i] == 'T')
		{
			_rna[i] = 'U';
		}
	}

	return _rna;
}

string Nucleus::get_reversed_DNA_strand() const
{
	string reverse_dna = "";

	for (int i = 0; i < _DNA_strand.size(); i++)
	{
		switch (_DNA_strand[i])
		{
		case 'G':
			reverse_dna += 'C';
		break;
		case 'C':
			reverse_dna += 'G';
		break;
		case 'A':
			reverse_dna += 'T';
		break;
		case 'T':
			reverse_dna += 'A';
		break;
		}
	}

	return reverse_dna;
}

unsigned int Nucleus::get_num_of_codon_appearances(const std::string& codon) const
{
	unsigned int number_of_found_codon = 0;

	size_t found_pos = _DNA_strand.find(codon);

	while (found_pos != string::npos)
	{
		number_of_found_codon++;
		found_pos = _DNA_strand.find(codon, found_pos + codon.size());
	}

	return number_of_found_codon;
}