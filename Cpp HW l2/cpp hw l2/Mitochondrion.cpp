#include "Mitochondrion.h"

void Mitochondrion::init()
{
	_glocuse_level = 0;
	_has_glocuse_receptor = false;
}

void Mitochondrion::insert_glucose_receptor(const Protein& protein)
{
	AminoAcidNode* curr = protein.get_first();

	if (curr->get_data() == ALANINE)
	{
		curr = curr->get_next();

		if (curr->get_data() == LEUCINE)
		{
			curr = curr->get_next();

			if (curr->get_data() == GLYCINE)
			{
				curr = curr->get_next();

				if (curr->get_data() == HISTIDINE)
				{
					curr = curr->get_next();

					if (curr->get_data() == LEUCINE)
					{
						curr = curr->get_next();

						if (curr->get_data() == PHENYLALANINE)
						{
							curr = curr->get_next();

							if (curr->get_data() == AMINO_CHAIN_END)
							{
								_has_glocuse_receptor = true;
							}
						}
					}
				}
			}
		}
	}

}

void Mitochondrion::set_glucose(const unsigned int glocuse_units)
{
	_glocuse_level = glocuse_units;
}

bool Mitochondrion::produceATP() const
{
	if (_has_glocuse_receptor == true && _glocuse_level >= 50)
	{
		return true;
	}
	else
	{
		return false;
	}
}