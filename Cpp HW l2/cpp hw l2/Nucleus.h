#ifndef NUCLEUS
#define NUCLEUS

#include <string>
#include <iostream>

using namespace std;

class Gene 
{

private:
	unsigned int _start;
	unsigned int _end;
	bool _on_complementary_dna_strand;
public:
	void init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand);

	unsigned int get_start() const;
	unsigned int get_end() const;
	bool is_on_complementary_dna_strand() const;

	void set_start(unsigned int _start);
	void set_end(unsigned int _end);
	void set_on_complementary_dna_strand(bool on_complementary_dna_strand);
};

class Nucleus
{

private:
	string _DNA_strand;
	string _complementary_DNA_strand;

public:
	void init(const std::string dna_sequence);
	string get_RNA_transcript(const Gene& gene) const;
	string get_reversed_DNA_strand() const;
	unsigned int get_num_of_codon_appearances(const std::string& codon) const;
};

#endif
